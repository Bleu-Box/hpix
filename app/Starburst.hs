module Starburst where

import Control.Monad.Reader
import Data.Colour
import Data.Colour.Names
import Hpix
import Numeric.Noise.Perlin
import System.Random (getStdRandom, randomR)

-- width, height, half-width, and half-height
w, h, hw, hh :: Float
(w, h, hw, hh) = (500, 500, w / 2, h / 2)

main :: IO ()
main = do
  seed <- getStdRandom $ randomR (1, 10000000)
  window "Starburst" w h (starburst seed)

-- Makes a really cool starburst pattern with a teal -> green color gradient
starburst :: Int -> RenderLoop
starburst s t = do
  clearTo black
  mapM_ pix' vals
    where vals = [(realToFrac $ 50.0 * noise s x y 1,
                   realToFrac $ 250.0 * noise s y x t)
                 | x <- [0..100], y <- [0..100]]

-- Uses polar coordinates to draw a pixel offset to the center of the screen,
-- which is also highlighted based on its position relative to the center.
pix' :: (Float, Float) -> RenderReader
pix' (theta, r) = pix (x, y) c
  where x = hw + ((*r) . cos $ theta)
        y = hh + ((*r) . sin $ theta)
        c = blend (r ** 1.35 / 800) lightgreen teal

-- Returns a 3-dimensional perlin noise generator given a seed
noise :: (Real a) => Int -> a -> a -> a -> Double
noise seed x y z = noiseValue gen (x', y', z')
  where gen = perlin seed 5 0.01 0.005
        -- Make sure the coordinates get converted to doubles.
        -- `noise` could just accept only Doubles, but it's better
        -- to be flexible here for convenience.
        x' = realToFrac x :: Double
        y' = realToFrac y :: Double
        z' = realToFrac z :: Double
    
