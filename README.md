# hpix

Hpix is a tiny library for creating simple pixel-based sketches.
It uses Haskell's SDL2 bindings for rendering.

## Running the examples
Example programs can be found in the app/ directory.
To run them, you need [stack](https://haskellstack.org/) installed.
If/when stack is installed, just enter the command
```
$ stack build && stack exec examples-all
```
## Creating your own sketches
First, make a window:

```haskell
import Data.Colour.Names
import Hpix

main :: IO ()
main = window "Title here" width height renderLoop
  where width = 500
        height = 500
```

Then make the rendering loop, which takes a time counter as an argument:

```
renderLoop :: RenderLoop
renderLoop time = do
   -- clear the screen to a solid black color
  clearTo black
  -- draw a cool-looking green thing
  mapM_ ((flip pix) green) [(x, 250 + 20 * Prelude.tan (x+time/500)) | x <- [0..500]]
```

And that's all there is to it! Hit `Escape` to exit the window. 