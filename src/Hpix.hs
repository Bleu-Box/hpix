{-# LANGUAGE OverloadedStrings #-}

-- | "Hpix" is a little library for drawing only with pixels.
-- It uses Haskell's SDL2 bindings for rendering, and uses
-- Data.Colour for colors. It's intended to be super minimal.

module Hpix ( RenderLoop
            , RenderReader
            , clearTo
            , pix
            , window ) where

import Control.Monad (unless)
import Control.Monad.Reader
import Data.Text (pack)
import Foreign.C.Types (CInt)
import SDL

import Draw
import Input

-- | `RenderReader` is like an IO monad mashed up with
-- a Reader that accepts an SDL2 Renderer. Functions
-- of this type (i.e. `Hpix.Draw.pix`) are used to draw to the screen.
type RenderReader = ReaderT Renderer IO ()

-- | A `RenderLoop` takes a time variable and returns a `RenderReader`
type RenderLoop = Float -> RenderReader

-- | Initializes a high-dpi SDL2 window given a width, height, and render loop.
-- The render loop takes a time variable.
window :: String -> Float -> Float -> RenderLoop -> IO ()
window title w h loop = do
  initializeAll
  -- make a high-dpi window with specified title & size
  window <- createWindow title' cfg
  ren <- createRenderer window (-1) defaultRenderer
  putStrLn "Opening window. Press <ESC> to quit."
  -- run the main loop at time=0 using the loop function provided
  runLoop loop 0 ren
    where cfg = defaultWindow { windowHighDPI = True, windowInitialSize = V2 w' h' }
          w' = round w :: CInt
          h' = round h :: CInt
          title' = pack title

-- | Runs the rendering loop over and over again, providing it with a time
-- and a Renderer, and closes the window when the Escape key is pressed.
runLoop :: RenderLoop -> Float -> Renderer -> IO ()
runLoop loop t ren = do
  -- Get the event list.
  evts <- pollEvents
  -- run & present the sketch
  runReaderT (loop t) ren
  present ren
  -- loop unless user presses Escape
  unless (any (isKeyPress KeycodeEscape) evts) (runLoop loop (t+1) ren)
