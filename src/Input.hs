-- | "Hpix.Input" is an internal module for handling user input.
-- It's very small because Hpix sketches aren't meant to be interactive.

module Input (Keycode(..), isKeyPress) where

import SDL.Event
import SDL.Input.Keyboard

-- | Looks at an event and returns whether the event
-- is a keypress for the specified key.
isKeyPress :: Keycode -> Event -> Bool
isKeyPress k e = case eventPayload e of
  KeyboardEvent kEvt ->
    keyboardEventKeyMotion kEvt == Pressed &&
    keysymKeycode (keyboardEventKeysym kEvt) == k
  _ -> False
