-- | "Hpix.Draw" contains drawing utilities for the
-- Spartan Hpix library.

module Draw (clearTo, pix) where

import Data.Colour (Colour(..))
import Data.Colour.SRGB (RGB(..), toSRGBBounded)
import Control.Monad.Reader
import Foreign.C.Types (CInt)
import SDL


-- | `RenderReader` is like an IO monad mashed up with
-- a Reader that accepts an SDL2 Renderer. Functions
-- of this type (i.e. `Hpix.Draw.pix`) are used to draw to the screen.
type RenderReader = ReaderT Renderer IO ()

-- | A type alias for the coordinates used by drawing functions.
type Coord = (Float, Float)

-- | Takes a location and a Colour, and then draws a pixel
-- with that color and location onto the window.
pix :: Coord -> Colour Float -> RenderReader
pix (x, y) c = do
  ren <- ask
  setColour c
  liftIO $ drawPoint ren (P $ V2 x' y')
    where x' = round x :: CInt
          y' = round y :: CInt

-- | Clears the window to the specified color.
clearTo :: Colour Float -> RenderReader
clearTo c = do
  ren <- ask
  setColour c
  clear ren

-- | Sets the current drawing color.
setColour :: Colour Float -> RenderReader
setColour c = do
  ren <- ask
  rendererDrawColor ren $= (colourToV4 c)

-- | Converts a Colour (used by Hpix) to a V4 (used by SDL2),
colourToV4 :: (RealFrac b, Floating b, Integral a, Bounded a) => Colour b -> V4 a
colourToV4 c = V4 r g b 255
  where (RGB r g b) = toSRGBBounded $ c
